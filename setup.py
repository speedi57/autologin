#!/usr/bin/python
# -*- coding: utf-8 -*-
 
# source d'inspiration: http://wiki.wxpython.org/cx_freeze
 
import sys, os
from cx_Freeze import setup, Executable
 
#############################################################################
# preparation des options 
path = sys.path
includes = []
excludes = []
packages = ["mechanize", "urllib2","os","cookielib","threading","time","configparser","sys","eventlet","datetime"]
includefiles = []


options = {"path": path,
           "includes": includes,
		   "include_files": includefiles,
           "excludes": excludes,
           "packages": packages
           }
 
#############################################################################
# preparation des cibles
base = None
if sys.platform == "win32":
    base = "console"
 
cible_1 = Executable(
    script = "autologin.py",
    base = base,
    compress = True,
    icon = None,
    )
 
#############################################################################
# creation du setup
setup(
    name = "autologin",
    version = "4",
    description = "connection auto crous YACAP",
    author = "speedi57",
    options = {"build_exe": options},
    executables = [cible_1]
    )